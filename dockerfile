# Imagem base do Wordpress
FROM wordpress:latest

# Adicione os arquivos do seu tema e plugins ao diretório de temas e plugins do Wordpress
COPY /wp-content/themes /var/www/html/wp-content/themes
COPY /wp-content/plugins /var/www/html/wp-content/plugins

# Copie o arquivo wp-config.php para o diretório raiz do Wordpress
COPY wp-config.php /var/www/html/

# Defina as variáveis de ambiente do Wordpress
ENV WORDPRESS_DB_HOST=<ENDEREÇO DO SEU BANCO DE DADOS> \
    WORDPRESS_DB_USER=<USUÁRIO DO BANCO DE DADOS> \
    WORDPRESS_DB_PASSWORD=<SENHA DO BANCO DE DADOS> \
    WORDPRESS_DB_NAME=<NOME DO BANCO DE DADOS>

# Exponha a porta do Wordpress
EXPOSE 80

# Inicie o servidor web do Wordpress
CMD ["apache2-foreground"]
